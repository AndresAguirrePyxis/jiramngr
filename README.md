# README #

Este backend tiene como objetivo abstraer los servicios de Jira y Tempo para ser utilizados para la generación automática
de reportes realizados por Pyxis Aqua.

### fucionamiento

El endpoint /api/worklogs devuelve una lista con la carga de horas sumarizada por la clave (usuario, proyecto), recibe
como path param las variables *to* y *from* que establecen el período de tiempo a ser tenido en cuenta.

ejemplo de invocación:
curl --location --request GET 'http://localhost:8080/api/worklogs?from=2021-09-01&to=2021-09-30'

#### generación de planilla .csv

* ejecutar el servicio

* ejecutar en una terminal:
    
    + cd assets
    
    + curl --location --request GET 'http://localhost:8080/api/worklogs?from=2021-09-01&to=2021-09-30' > getWorklogs.json ; python3 json2csv.py
    
En el archivo getWorklogs.csv se encuentra la salida esperada, tener en cuenta modificar el parámetro *from* y *to*.

### setup

Para autenticarse con la api de tempo se esta utilizando un token asociado con el usuario, por lo que los permisos sobre
los worklogs tienen que ver con los permisos del usuario utilizado para obtener el token.

Puede modificarse el token mediante la constante *tempo.user.token*.

Para obtener el token se debe de ir a API integration dentro de los settings de tempo.

Como trabajo futuro se plantea la utilización de OAUTH en lugar de un token asociado a un usuario dado.

El backend requiere Java 8 o superior y maven, para la generación de la planilla csv se requiere python 3.


