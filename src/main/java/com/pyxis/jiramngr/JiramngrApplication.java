package com.pyxis.jiramngr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JiramngrApplication {

	public static void main(String[] args) {
		SpringApplication.run(JiramngrApplication.class, args);
	}

}
