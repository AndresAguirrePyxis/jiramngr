package com.pyxis.jiramngr.controllers;

import com.pyxis.jiramngr.dtos.WorkLogSumarizedDto;
import com.pyxis.jiramngr.services.JiraTempoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Slf4j
public class WorkloadController {
    @Autowired
    private JiraTempoService jiraTempoService;

    @RequestMapping(method = RequestMethod.GET, path = "/api/worklogs")
    public ResponseEntity<List<WorkLogSumarizedDto>> getWorklogs(@RequestParam String from , String to){
        List<WorkLogSumarizedDto> workloads = jiraTempoService.getWorklogs(from, to);
        return new ResponseEntity<>(workloads, HttpStatus.OK);
    }

}
