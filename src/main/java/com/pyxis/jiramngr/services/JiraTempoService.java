package com.pyxis.jiramngr.services;

import com.pyxis.jiramngr.dtos.MetadataDto;
import com.pyxis.jiramngr.dtos.WorkLogSumarizedDto;
import com.pyxis.jiramngr.dtos.WorklogDto;
import com.pyxis.jiramngr.dtos.WorklogsDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class JiraTempoService {

    @Value("${tempo.user.token}")
    private String tempoUserToken;

    @Value("${tempo.worklog.endpoint}")
    private String tempoWorklogEndpoint;

    @Value("${tempo.worklog.pagesize:50}")
    private int tempoWorklogPageSize;

    // helper function
    public static <T> Predicate<T> distinctByKey(
            Function<? super T, ?> keyExtractor) {

        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }
    // out: usuario, proyecto, mes, billablehours, nonbillablehours, totalhours = sum(billablehours, nonbillablehours)
    public List<WorkLogSumarizedDto> getWorklogs(String from, String to) {
        HttpHeaders tempoHeaders = new HttpHeaders();
        tempoHeaders.setContentType(MediaType.APPLICATION_JSON);
        tempoHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        tempoHeaders.set("Authorization", "Bearer " + tempoUserToken);
        HttpEntity request = new HttpEntity<>(tempoHeaders);
        ResponseEntity<WorklogsDto> response;
        List<WorklogDto> worklogs = new ArrayList<>();
        try {
            RestTemplate restTemplate = new RestTemplate();
            response = restTemplate.exchange(tempoWorklogEndpoint + "?from=" + from + "&to=" + to +
                            "&offset=0&limit=" + String.valueOf(tempoWorklogPageSize), HttpMethod.GET,
                    request,
                    WorklogsDto.class);
            int offset = 0;
            MetadataDto metadata = response.getBody().getMetadata();
            worklogs = response.getBody().getResults();

            while(metadata.getNext()!=null){

                offset = offset + tempoWorklogPageSize;
                response = restTemplate.exchange(tempoWorklogEndpoint + "?from=" + from + "&to=" + to +
                                "&offset=" + offset + "&limit=" + String.valueOf(tempoWorklogPageSize), HttpMethod.GET,
                        request,
                        WorklogsDto.class);
                worklogs.addAll(response.getBody().getResults());
                metadata = response.getBody().getMetadata();
            }
        } catch (RestClientException e) {
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        List<WorklogDto> worklogListNewKey = worklogs.stream().map(w -> {
            // Se que el nombre de la issue tiene como prefijo "nombre_proyecto-"
            String key = w.getIssue().getKey().split("\\s*-\\s*")[0];
            w.setDescription("Sumarización de horas para " + w.getAuthor().getDisplayName() + " en el proyecto " + key);
            w.setProject(key);
            key = w.getAuthor().getAccountId() + key;
            w.getIssue().setKey(key);
            return w;
        }).collect(Collectors.toList());

        List<WorklogDto> distinctWl = worklogListNewKey.stream().filter(distinctByKey(w -> w.getIssue().getKey())).
                collect(Collectors.toList());
        List<WorkLogSumarizedDto> sumarizedWl = distinctWl.stream().map(
            w1 -> {
                WorkLogSumarizedDto wres = new WorkLogSumarizedDto();
                List<WorklogDto> worklogsByKey = worklogListNewKey.stream().
                        filter(w2 -> w2.getIssue().getKey().equals(w1.getIssue().getKey())).collect(Collectors.toList());
                int billableSecondsByKey = worklogsByKey.stream().reduce(0, (subtotal, workload) ->
                        subtotal + workload.getBillableSeconds(), Integer::sum);
                int timeSpentSecondsByKey = worklogsByKey.stream().reduce(0, (subtotal, workload) ->
                        subtotal + workload.getTimeSpentSeconds(), Integer::sum);
                wres.setBillableSeconds(billableSecondsByKey);
                wres.setTimeSpentSeconds(timeSpentSecondsByKey);
                wres.setAuthor(w1.getAuthor());
                wres.setProject(w1.getProject());
                wres.setAuthorName(w1.getAuthor().getDisplayName());
                return wres;
            }
        ).collect(Collectors.toList());

        Collections.sort(sumarizedWl, (s1, s2) -> s1.getAuthorName().compareTo(s2.getAuthorName())
                > 1 ? 1 : s1.getAuthorName().compareTo(s2.getAuthorName()) < 1 ? -1 : 0);

        return sumarizedWl;
    }
}
