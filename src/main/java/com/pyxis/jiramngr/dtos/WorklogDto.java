package com.pyxis.jiramngr.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WorklogDto {
    private String self;
    private String tempoWorklogId;
    private String jiraWorklogId;
    private IssueDto issue;
    private int timeSpentSeconds;
    private int billableSeconds;
    private String startDate;
    private String startTime;
    private String description;
    private String createdAt;
    private String updatedAt;
    private AuthorDto author;
    private String project;
}
