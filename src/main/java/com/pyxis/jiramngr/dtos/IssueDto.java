package com.pyxis.jiramngr.dtos;

import lombok.Data;

@Data
public class IssueDto {
    String self;
    String key;
    int id;
}
