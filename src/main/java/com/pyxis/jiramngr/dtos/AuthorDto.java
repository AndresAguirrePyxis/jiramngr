package com.pyxis.jiramngr.dtos;

import lombok.Data;

@Data
public class AuthorDto {
    String self;
    String accountId;
    String displayName;
}

