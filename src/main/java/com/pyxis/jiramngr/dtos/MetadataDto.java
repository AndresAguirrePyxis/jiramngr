package com.pyxis.jiramngr.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MetadataDto {
    int count;
    int offset;
    int limit;
    String previous;
    String next;
}
