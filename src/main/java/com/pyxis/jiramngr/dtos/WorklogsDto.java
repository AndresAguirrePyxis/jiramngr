package com.pyxis.jiramngr.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WorklogsDto {
    List<WorklogDto> results;
    MetadataDto metadata;
}
