package com.pyxis.jiramngr.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WorkLogSumarizedDto {
    private String authorName;
    private String project;
    private int billableSeconds;
    private int timeSpentSeconds;
    private AuthorDto author;
}
